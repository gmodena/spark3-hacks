SHELL := /bin/bash

spark_version := 3.2.0
hadoop_version := hadoop2.7
hadoop_conf_dir := /etc/hadoop/conf/
hive_conf_dir :=/etc/spark2/conf/
spark_distribution := spark-${spark_version}-bin-${hadoop_version}
pyspark_submit_args :='pyspark-shell'
project_name := myspark3
project := $(shell echo ${HOME})/${project_name}
spark_home := ${project}/spark3
venv_path := ${project}/venv
stat_host=stat1005.eqiad.wmnet



project:
	test -d ${project} || mkdir -p ${project}

venv: project
	cd ${project}; python3 -m venv venv

toree_install: venv
	source ${venv_path}/bin/activate;
	pip install -r requirements.txt;
	jupyter toree install --sys-prefix --kernel_name=toree --spark_home=${spark_home}

spark3_install: project
	# Fetch & uncompress a spark3 tarball from spark.apache.org. The resulting directory
	# is where SPARK_HOME needs to point to.
	# TODO(gmodena): we could pip or conda install spark3 in an env with
	# pip install spark==2.4
	# or
	# conda install spark==2.4.5
	#
	# We follow this approach in platform-airflow-dags, but there the only distro is spark 2.4.
	# Installing in a conda env  on a stats node would require:
	# 1. Setting SPARK_HOME to point to wherever in FHS conda installs the distribution
	# 2. Setting SPARK_CONF_DIR to point to wherever we store our custom config.
	#
	cd ${project}
	wget https://dlcdn.apache.org/spark/spark-${spark_version}/${spark_distribution}.tgz
	tar xzf ${spark_distribution}.tgz
	mv ${spark_distribution} ${project}/spark3
	rm ${spark_distribution}.tgz

install: project venv toree_install spark3_install
	ls ${spark_home}
	cp conf/spark-defaults.conf ${spark_home}/conf
	cp /etc/spark2/conf/hive-site.xml ${spark_home}/conf
	cat /etc/spark2/conf/log4j.properties conf/log4j.properties >> ${spark_home}/conf/log4j.properties
	cp -r notebooks ${project}

uninstall:
	source ${venv_path}/bin/activate
	jupyter kernelspec uninstall toree_scala
	rm -r ${project}

jupyter_server:
	bash -c "source ${venv_path}/bin/activate; \
		export SPARK_HOME=${spark_home}; \
		export HADOOP_CONF_DIR=${hadoop_conf_dir};  \
		export PYSPARK_SUBMIT_ARGS=${pyspark_submit_args};  \
		export TOREE_OPTS='--nosparkcontext'; \
		jupyter lab --no-browser --notebook-dir=${project}/notebooks"

