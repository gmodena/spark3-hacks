# spark3-hacks

A collection of hacks to run DIY spark3 binaries in Jupyter Lab on the Analytics infra.

WARNING: this is *not* an official project, and no support is provided. Expect things to break.

# What

The project `Makefile` inits a Spark3 project under `${HOME}/myspark3`.

It installs Spark3 with Metastore (Hive) integration, and provides a Jupyter Lab instance,
with pyspark and Scala support, kernel inside a virtual environment.

## Caveats

Dynamic allocation and shuffle service must be turned off:
```
spark.dynamicAllocation.enabled                     false
spark.shuffle.service.enabled                       false
```

`conf/spark-defaults.conf` provides a basic config, modified from the default 2.4 one.

# Getting started

Clone this repo on a target host (e.g. stat1005.eqiad.wmnet), and hop onto it over ssh.


Init a $HOME/myspark3 project with:
```
make install
```

Launch a jupyter lab server with:
```
make jupyter_server
```

This command should produce something like:
```
[...]
    To access the server, open this file in a browser:
        file:///srv/home/gmodena/.local/share/jupyter/runtime/jpserver-8432-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/lab?token=65c8b2f40f8df74a8a3c86d861ea7b00a2c4969495855331
     or http://127.0.0.1:8888/lab?token=65c8b2f40f8df74a8a3c86d861ea7b00a2c4969495855331
```

You'll need to establish a ssh tunnel from your local machine to the jupyter lab instance.
For example (from your laptop
```
ssh -t -N -L 8888:127.0.0.1:8888 stat1005.eqiad.wmnet
```
Now you can access http://localhost:8888/lab?token=65c8b2f40f8df74a8a3c86d861ea7b00a2c4969495855331 from the browser.

# Uninstall

The following command will get rid of project files, python venv, and spark3 binaries:
```
make uninstall
```
